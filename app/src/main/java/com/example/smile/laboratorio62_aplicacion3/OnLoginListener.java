package com.example.smile.laboratorio62_aplicacion3;

public interface OnLoginListener {
    void onLogin(String usuario, String password);
}
